﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_CLIENTE.Models.Validations
{
    [MetadataType(typeof(Cliente.MetaData))]
    public partial class Cliente
    {
        sealed class MetaData
        {
            [Key]
            public int IdCliente;

            [Required(ErrorMessage = "Ingrese el nombre de cliente")]
            public string NombreCliente;

            [Required(ErrorMessage = "Ingrese email válido")]
            public string Correo;

            [Required]
            [Range(20, 50, ErrorMessage = "Edad entre 20 y 50")]
            public Nullable<int> EdadCliente;

            [Required(ErrorMessage = "Ingrese salario")]
            public Nullable<int> CreditoCliente;
        }
    }
}